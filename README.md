# This Repository is *deprecated*
Please open issues in https://gitlab.com/gitlab-com/support/support-team-meta/issues
and use the `dotcom-meta` tag.